#############################################################################
# Copyright (C) 2015  Rafal Kobel <rafyco1@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Rafal Kobel <rafyco1@gmail.com>
# Project: 
# Description:
#############################################################################


PYTHON=python
MANAGE=$(PYTHON) manage.py
SETUP=$(PYTHON) setup.py
RM=rm -rf
COPY=cp
DOC=epydoc
PYLINT=pylint
PYLINT_FLAGS=-f parseable -d I0011,R0801,R0921 --extension-pkg-whitelist=PyQt4 --ignore runtests.py

DOC_FILE=ajango_docs
DOC_PDF=dist
PLINT_OUT=pylint.out

SCREEN=screen
SCREEN_NAME=ajango2_dflasgfahgasjkds
DEAMON_PORT=8129

#############################################################################

all: test documentation build clean

ifeq ($(OS),Windows_NT)

build: build-example build-tgz build-wnd build-dumb
	@echo "Zbudowano elementy"
build-wnd:
	@$(SETUP) bdist_wininst
	@echo "Zbudowano instalator dla platformy Windows"

else

build: build-example build-tgz build-dumb
	@echo "Zbudowano elementy"

endif

build-tgz:
	@$(SETUP) bdist
	@echo "Zbudowano pakiet tgz"

build-example: test remove
	echo Budowanie przykladowego programu
	$(COPY) example/manage.py .
	$(COPY) -r example/project .
	test -f db.sqlite3 || cp example/db.sqlite3 .
	test -f skeleton.xml || cp example/skeleton.xml .
	$(MANAGE) generateapps

build-dumb:
	@$(SETUP) bdist_dumb

test:
	@$(PYTHON) -m ajango.tests -v

documentation: doc-pdf doc-html

doc-pdf:
	@$(DOC) -v -o $(DOC_PDF) --pdf --name "Ajango Documentation" ajango

doc-html:
	@$(DOC) -v ajango --html -o $(DOC_FILE)
	@$(COPY) $(DOC_FILE)/module-tree.html $(DOC_FILE)/index.html

pylint:
	@$(PYLINT) $(PYLINT_FLAGS) ajango

clean:
	@$(RM) $(DOC_PDF)/*.tex
	@$(RM) *.aux *.idx *.log *.out *.toc *.dvi
	@$(RM) $(DOC_PDF)/*.aux $(DOC_PDF)/*.idx $(DOC_PDF)/*.log $(DOC_PDF)/*.out build $(DOC_PDF)/*.toc $(DOC_PDF)/*.dvi
	@$(RM) */__pycache__

remove: clean
	@echo Usuwanie wygenerowanych plikow
	@$(RM) project blog mynewapp $(DOC_PDF) $(DOC_FILE)    
	@$(RM) skeleton.xml db.sqlite3 manage.py Ajango.egg-info
	@$(RM) dist

run:
	@$(MANAGE) runserver 0:8000

deamon-start:
	@$(SCREEN) -dmS $(SCREEN_NAME) $(MANAGE) runserver 0:$(DEAMON_PORT)
	@echo "Deamon z aplikacja testowa uruchomiony"

deamon-stop:
	@$(SCREEN) -X -S $(SCREEN_NAME) quit; exit 0
	@echo "Deamon wylaczony"

install:
	@$(SETUP) install

jenkins-test: remove documentation build clean
	py.test --junitxml test-results.xml ajango/tests/__main__.py

help:
	@echo "Lista dostepnych polecen."
	@echo " "
	@echo "install       - Instalacja pakietu Ajango"
	@echo "build         - Tworzenie plikow budowania"
	@echo "build-example - Budowanie przykladowego projektu"
	@echo "test          - Uruchomienie testow pakietu"
	@echo "documentation - Budowanie dokumentacji."
	@echo "doc-pdf       - Budowanie dokumentacji pdf"
	@echo "doc-html      - Budowanie dokumentacji html"
	@echo "run           - Uruchomienie serwera testowego"
	@echo "clean         - Usuniecie plikow budowania"
	@echo "remove        - Wyczyszczenie repozytorium ze zbednych plikow"
	@echo "jenkins-test  - Zadania dla mechanizmow Jenkinsa"
	@echo "deamon-start  - Uruchomienie aplikacji testowej na porcie $(DEAMON_PORT)"
	@echo "deamon-stop   - Wylaczenie aplikacji testowej"
