#!/bin/bash

mkdir -r dist

epydoc -v -o dist --pdf --name "Ajango Documentation" ajango
cd dist
echo -ne '\n\n' | pdflatex api.tex -interaction scrollmode
echo -ne '\n\n' | pdflatex api.tex -interaction scrollmode
cd -

mv api.pdf dist/
#rm *.aux *.idx *.out *.toc *.dvi
rm dist/*.aux dist/*.idx dist/*.out dist/*.toc dist/*.dvi
exit 0

