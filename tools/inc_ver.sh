#! /bin/sh
# copyright (c) Rafal Koble <rafyco1@gmail.com>
# This script incremented version of software
# It could be use as pre commit hooks

VER_FILE="ajango/__init__.py README.md"
VER=$(python -m ajango -v | awk '{ print $1 }')

FIRST=$(echo $VER | sed "s/\./ /g" | awk '{ print $1 }')
SECOND=$(echo $VER | sed "s/\./ /g" | awk '{ print $2 }')
THIRD=$(echo $VER | sed "s/\./ /g" | awk '{ print $3 }')

THIRD=$(( $THIRD + 1 ))

NEW=$(echo $FIRST.$SECOND.$THIRD)

for ONCE in $VER_FILE; do
    sed -i s/$VER/$NEW/ $ONCE
done

git commit -m "increment version from $VER to $NEW" $VER_FILE
git tag -a v$VER_FILE -m "wersja $VER_FILE"
