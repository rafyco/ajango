# Ajango framework

![logo](tools/img/logo1.png)

Program został przygotowany w ramach pracy magisterskiej.

[![Build Status](https://img.shields.io/jenkins/s/https/site.rafyco.pl/jenkins/Ajango.svg?maxAge=2592000)](http://site.rafyco.pl/jenkins/job/Ajango/)
[![Tests Status](https://img.shields.io/jenkins/t/http/site.rafyco.pl/jenkins/ajango.svg?maxAge=2592000)](http://site.rafyco.pl/jenkins/job/Ajango/lastCompletedBuild/testReport/)
[![Bitbucket issue](https://img.shields.io/bitbucket/issues/rafyco/ajango.svg?maxAge=2592000)](https://bitbucket.org/rafyco/ajango/issues?status=new&status=open)
[![Website](https://img.shields.io/website-up-down-green-red/http/site.rafyco.pl%3A8129.svg)](http://site.rafyco.pl:8129)
[![Author](https://img.shields.io/badge/author-Rafa%C5%82%20Kobel-orange.svg)](http://rafyco.pl/)
![version](https://img.shields.io/badge/version-1.0.0-ff69b4.svg)
[![License](https://img.shields.io/badge/license-GNU-blue.svg)](http://www.gnu.org/licenses/gpl.html)

## Opis

Moduł języka Python umożliwiający automatyzacje tworzenia aplikacji typu __'light of buissnes'__.

Do poprawnego uruchomienia aplikacji niezbędne jest środowisko Django.

## Przydatne linki

 * ![jenkins][jenkin] [Jenkins](http://site.rafyco.pl/jenkins)
 * ![bitbucket][bbucket] [Bitbucket](http://bitbucket.org/rafyco/ajango.git)
 * ![documentation][doc] [Dokumentacja](http://site.rafyco.pl/jenkins/job/Ajango/javadoc)


## Uruchomienie projektu 'przykładowego' bez instalacji biblioteki

Dostępne w tej sekcji polecenia __nie wymagają__ instalacji biblioteki __Ajango__. W przypadku jej braku w systemie
zostaną użyte pliki znajdujące się w katalogu repozytorium.

W celu zbudowania i przetestowania oprogramowania można skorzystać ze zdefiniowanych w pliku _Makefile_ poleceń.
Listę dostępnych czynności można otrzymać poprzez wywołanie polecenia:

	make help

### Budowanie programu

Budowanie składa się z automatycznego skopiowania plików projektowych, znajdujących się w folderze example oraz 
wykonania polecenia ```python manage.py generateapps``` budującego aplikację z pliku szkieletowego. Można to
uzyskać wykonując polecenie:

	make build-example

Polecenie utworzy nowy katalog o nazwie _project_ z projektem Django. Utworzone zostaną również foldery o nazwach: 
_blog_ oraz _mynewapp_. Znajdują się w nich wygenerowane aplikacje. Wartym uwagi jest też plik _skeleton.xml_
Z którego zostały wygenerowane aplikacje.

### Uruchomienie serwera testowego

Chcąc uruchomić serwer testowy można skorzystać z polecenia:

	make run

Tworzy one serwer na porcie 8000. W celu obejrzenia efektu pracy należy w dowolnej wyszukiwarce wpisać adres:
[http://localhost:8000/](http://localhost:8000)

### Czyszczenie repozytorium

W celu ponownej kompilacji projektu należy przed ponownym budowaniem usunąć wygenerowane pliki. Można to uzyskać
poleceniem:

	make remove


## Instalacja

Istnieją dwa sposoby instalacji bibliotek __Ajango__:

### Z użyciem programu make

    make install
    
### Za pomocą skrytpu setup.py

    python setup.py install
 
## Pierwsze użycie

Tworzymy projekt frameworka Django.

    python django-admin.py startproject <project> .

W pliku __setting.py__ dodajemy do zmiennej ```INSTALLED_APPS``` opcje __'ajango.contrib.automatic'__.

Uruchamiamy opcje tworzącą plik szkieletowy lub kopiujemy wcześniej przygotowany plik do folderu z projektem:

    python manage.py makeskeleton

Edytujemy plik szkieletu opisującego aplikacje. Operacje te wykonywane są według uznania i potrzeb programisty.

Generujemy aplikacje poleceniem:

    python manage.py generateapps

Aplikacje zostaną wygenerowane w pliku roboczym w postaci folderów o odpowiednich nazwach. Oprócz tego zostaną uzupełnione również pliki z kodem potrzebne do jej uruchomienia. W celu przetestowania powstałego produktu należy wykonać polecenie uruchamiające serwer testowy pakietu Django.

Więcej informacji dostępne w [Dokumentacji](http://site.rafyco.pl/jenkins/job/Ajango/javadoc)

## Autor

Rafał Kobel <rafyco1@gmail.com>

## Licencja

>    Copyright (C) 2016  Rafal Kobel <rafyco1@gmail.com>
>
>    This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU General Public License as published by
>    the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU General Public License for more details.
>
>    You should have received a copy of the GNU General Public License
>    along with this program.  If not, see <http://www.gnu.org/licenses/>.


[doc]: https://cdn0.iconfinder.com/data/icons/customicondesign-office7-shadow-png/16/Product-documentation.png

[jenkin]: https://courses.cs.washington.edu/courses/cse403/13sp/images/icon_jenkins.gif

[bbucket]: http://iconshow.me/media/images/social/flat-gradient-social-media-icons/png/16/Bitbucket.png

[pdf]: https://cdn1.iconfinder.com/data/icons/CrystalClear/16x16/mimetypes/pdf.png